CC=gcc
CFLAGS=-lm -I./includes
DEPS=point.h
OBJ=main.o Point.o

all:makeprograma

makeprograma: $(OBJ)
	$(CC) -o makeprograma $(OBJ) $(CFLAGS)

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -f makeprograma *.o
