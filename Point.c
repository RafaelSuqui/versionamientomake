
#include<Point.h>

/*funcion que calcula la  distancia euclidiana entre dos puntos*/
float DistanciaEuclidiana(Point point1, Point point2)
{
	float x1=0;
	float y2=0;
	float z3=0;
	float cuadrados=0;
	float resul=0;
	x1=(point2.x)-(point1.x);
	y2=(point2.y)-(point1.y);
	z3=(point2.z)-(point1.z);
	resul=sqrt(pow(x1,2)+pow(y2,2)+pow(z3,2));
	return resul;
}

/*Calculo del punto medio entre dos puntos en el espacio*/
Point punto_medio(Point a, Point b)
{
	float Xm=0;
	float Ym=0;
	float Zm=0;
	Xm=(a.x+b.x)/2;
	Ym=(a.y+b.y)/2;
	Zm=(a.z+b.z)/2;
	printf("2) PUNTO MEDIO COORDENADAS Pm:(%.2f , %.2f , %.2f)\n",Xm,Ym,Zm);
}
