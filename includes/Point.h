/*
EJERCICIO DE DISTANCIA EUCLIDIANA
*/
#include<math.h>
#include<stdio.h>


/* Estructura qu e contiene las coordenadas del punto */
typedef struct Point{
	float x;
	float y;
	float z;	
}Point;

/* funcion que retorna la distancia entre dos puntos en el espacio*/
float  DistanciaEuclidiana(Point point1, Point point2);

/* Funcion que calcula el punto medio entre dos puntos en el espacio*/
Point punto_medio(Point a, Point b);
